#!/usr/bin/python

Libraries = ["./cutinfo_nobarcode/AYWGO.cutinfo.txt","./cutinfo_nobarcode/AYWHA.cutinfo.txt","./cutinfo_nobarcode/AYWHB.cutinfo.txt","./cutinfo_nobarcode/AYWHC.cutinfo.txt","./cutinfo_nobarcode/AYWHG.cutinfo.txt","./cutinfo_nobarcode/AYWHH.cutinfo.txt","./cutinfo_nobarcode/AYWHN.cutinfo.txt","./cutinfo_nobarcode/AYWHO.cutinfo.txt","./cutinfo_nobarcode/AYWHP.cutinfo.txt","./cutinfo_nobarcode/AYWCW.cutinfo.txt","./cutinfo_nobarcode/AYWCX.cutinfo.txt","./cutinfo_nobarcode/AYWCY.cutinfo.txt","./cutinfo_nobarcode/AYWCZ.cutinfo.txt","./cutinfo_nobarcode/AYWGA.cutinfo.txt","./cutinfo_nobarcode/AYWGB.cutinfo.txt","./cutinfo_nobarcode/AYWGC.cutinfo.txt","./cutinfo_nobarcode/AYWGG.cutinfo.txt","./cutinfo_nobarcode/AYWGH.cutinfo.txt","./cutinfo_nobarcode/AYWGN.cutinfo.txt","./cutinfo_nobarcode/AYWGP.cutinfo.txt","./cutinfo_nobarcode/AYWGS.cutinfo.txt","./cutinfo_nobarcode/AYWGT.cutinfo.txt","./cutinfo_nobarcode/AYWGU.cutinfo.txt","./cutinfo_nobarcode/AYWGW.cutinfo.txt","./cutinfo_nobarcode/AYWGX.cutinfo.txt","./cutinfo_nobarcode/AYWGY.cutinfo.txt","./cutinfo_nobarcode/AYWGZ.cutinfo.txt"]
Library_count = 0
w,h = 70,27
Barcode_matrix = [[0 for x in range(w)] for y in range(h)]

Cutinfo = open("./cutinfo_nobarcode_data.txt","w+")

for lib in Libraries: 
    CutInfo=open(lib,"r")
    Barcode_dict = {}
    for lines in CutInfo:
        line = lines.strip("\n")
        line = line.split("\t")
        barcode = line[5]
        Length = len(barcode)
        if Length >= 70:
            print "100"
            if Length in Barcode_dict:
                Barcode_dict[70] += 1
            else:
                Barcode_dict[70] = 1
        else:
            if Length in Barcode_dict:
                Barcode_dict[Length] += 1
            else:
                Barcode_dict[Length] = 1
    #for length,count in Barcode_dict.items():
        #Count_File1.write(str(length) + "\t" + str(count) + "\n")
    for key in Barcode_dict:
        Barcode_matrix[Library_count][key] = Barcode_dict[key]
    Library_count += 1

for lib in Barcode_matrix:
    Cutinfo.write(str(lib) + "\n")




# Line_count=0
# Lib_Count=0
# Adapter_Count = 0

# for lines in Barcodes:
#     line=lines.strip("\n")
#     Line_count += 1
#     if line == "cutadapt version 1.2.1":
#         Line_count = -3
#     if Line_count == -2:
#         Library = line[153:158]
#         Count_File1.write(Library + "\t" + Library + "\n")
#         Count_File2.write(Library + "\t" + Library + "\n")
#     if line == "=== Adapter 1 ===":
#         Line_count = 0
#         Adapter_Count = 0
#     if line == "=== Adapter 2 ===":
#         Line_count = 0
#         Adapter_Count += 1
#     if Line_count >= 11:
#         if Adapter_Count == 0:
#             if line == "Lengths of removed sequences (3' or within)" or line == "length	count	expected	max. errors" or line == "":
#                 continue
#             else:
#                 line = line.split("\t")
#                 Length=line[0]
#                 Count=line[1]
#                 Count_File1.write(Length + "\t" + Count + "\n")
#         if Adapter_Count == 1:
#             if line == "Lengths of removed sequences (3' or within)" or line == "length	count	expected	max. errors" or line == "":
#                 continue
#             else:
#                 line = line.split("\t")
#                 Length=line[0]
#                 Count=line[1]
#                 Count_File2.write(Length + "\t" + Count + "\n")
            
