#!/bin/bash

module load cutadapt

for file in $(<Libraries.txt); do
	cutadapt -b CVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_nobarcode/$file.cutinfo.txt" "../Fungal_First_Run/$file/$file.fastq" > "./$file.sequences.cutadapt.fasta"
done

#cutadapt -b CVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWGO.cutinfo.txt" "../Fungal_First_Run/AYWGO/AYWGO.fastq" > "./AYWGO.sequences.cutadapt.fasta"

# cutadapt -b CVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWHA.cutinfo.txt" "../Fungal_First_Run/AYWHA/AYWHA.fastq" > "./AYWHA.sequences.cutadapt.fasta"

# cutadapt -b CVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWHB.cutinfo.txt" "../Fungal_First_Run/AYWHB/AYWHB.fastq" > "./AYWHB.sequences.cutadapt.fasta"

# cutadapt -b CNNNNNNVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNNNNNNVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWHC.cutinfo.txt" "../Fungal_First_Run/AYWHC/AYWHC.fastq" > "./AYWHC.sequences.cutadapt.fasta"

# cutadapt -b CNNNNNNVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNNNNNNVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWHG.cutinfo.txt" "../Fungal_First_Run/AYWHG/AYWHG.fastq" > "./AYWHG.sequences.cutadapt.fasta"

# cutadapt -b CNNNNNNVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNNNNNNVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWHH.cutinfo.txt" "../Fungal_First_Run/AYWHH/AYWHH.fastq" > "./AYWHH.sequences.cutadapt.fasta"

# cutadapt -b CNNNNNNNVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNNNNNNNVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWHN.cutinfo.txt" "../Fungal_First_Run/AYWHN/AYWHN.fastq" > "./AYWHN.sequences.cutadapt.fasta"

# cutadapt -b CNNNNNNNVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNNNNNNNVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWHO.cutinfo.txt" "../Fungal_First_Run/AYWHO/AYWHO.fastq" > "./AYWHO.sequences.cutadapt.fasta"

# cutadapt -b CNNNNNNNVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNNNNNNNVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWHP.cutinfo.txt" "../Fungal_First_Run/AYWHP/AYWHP.fastq" > "./AYWHP.sequences.cutadapt.fasta"

# cutadapt -b CNNNNNNNNVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNNNNNNNNVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWCW.cutinfo.txt" "../Fungal_First_Run/AYWCW/AYWCW.fastq" > "./AYWCW.sequences.cutadapt.fasta"

# cutadapt -b CNNNNNNNNVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNNNNNNNNVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWCX.cutinfo.txt" "../Fungal_First_Run/AYWCX/AYWCX.fastq" > "./AYWCX.sequences.cutadapt.fasta"

# cutadapt -b CNNNNNNNNVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNNNNNNNNVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWCY.cutinfo.txt" "../Fungal_First_Run/AYWCY/AYWCY.fastq" > "./AYWCY.sequences.cutadapt.fasta"

# cutadapt -b CNTNTNTNTNTNTNTNTVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNANANANANANANANAVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWCZ.cutinfo.txt" "../Fungal_First_Run/AYWCZ/AYWCZ.fastq" > "./AYWCZ.sequences.cutadapt.fasta"

# cutadapt -b CNTNTNTNTNTNTNTNTVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNANANANANANANANAVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWGA.cutinfo.txt" "../Fungal_First_Run/AYWGA/AYWGA.fastq" > "./AYWGA.sequences.cutadapt.fasta"

# cutadapt -b CNTNTNTNTNTNTNTNTVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNANANANANANANANAVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWGB.cutinfo.txt" "../Fungal_First_Run/AYWGB/AYWGB.fastq" > "./AYWGB.sequences.cutadapt.fasta"

# cutadapt -b CNTTNTTNTTNTTNTTNTTNTTNTTVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNAANAANAANAANAANAANAANAAVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWGC.cutinfo.txt" "../Fungal_First_Run/AYWGC/AYWGC.fastq" > "./AYWGC.sequences.cutadapt.fasta"

# cutadapt -b CNTTNTTNTTNTTNTTNTTNTTNTTVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNAANAANAANAANAANAANAANAAVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWGG.cutinfo.txt" "../Fungal_First_Run/AYWGG/AYWGG.fastq" > "./AYWGG.sequences.cutadapt.fasta"

# cutadapt -b CNTTNTTNTTNTTNTTNTTNTTNTTVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNAANAANAANAANAANAANAANAAVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWGH.cutinfo.txt" "../Fungal_First_Run/AYWGH/AYWGH.fastq" > "./AYWGH.sequences.cutadapt.fasta"

# cutadapt -b CNTTTNTTTNTTTNTTTNTTTNTTTNTTTNTTTVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNAAANAAANAAANAAANAAANAAANAAANAAAVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWGN.cutinfo.txt" "../Fungal_First_Run/AYWGN/AYWGN.fastq" > "./AYWGN.sequences.cutadapt.fasta"

# cutadapt -b CNTTTNTTTNTTTNTTTNTTTNTTTNTTTNTTTVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNAAANAAANAAANAAANAAANAAANAAANAAAVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWGP.cutinfo.txt" "../Fungal_First_Run/AYWGP/AYWGP.fastq" > "./AYWGP.sequences.cutadapt.fasta"

# cutadapt -b CNTTTNTTTNTTTNTTTNTTTNTTTNTTTNTTTVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNAAANAAANAAANAAANAAANAAANAAANAAAVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWGS.cutinfo.txt" "../Fungal_First_Run/AYWGS/AYWGS.fastq" > "./AYWGS.sequences.cutadapt.fasta"

# cutadapt -b CNNNNTTTTTTTTTTNNNNVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNNNNAAAAAAAAAANNNNVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWGT.cutinfo.txt" "../Fungal_First_Run/AYWGT/AYWGT.fastq" > "./AYWGT.sequences.cutadapt.fasta"

# cutadapt -b CNNNNTTTTTTTTTTNNNNVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNNNNAAAAAAAAAANNNNVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWGU.cutinfo.txt" "../Fungal_First_Run/AYWGU/AYWGU.fastq" > "./AYWGU.sequences.cutadapt.fasta"

# cutadapt -b CNNNNTTTTTTTTTTNNNNVTTTTTTTTTTTTTTTTTTTTTTTTTTTVN -b GNNNNAAAAAAAAAANNNNVAAAAAAAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWGW.cutinfo.txt" "../Fungal_First_Run/AYWGW/AYWGW.fastq" > "./AYWGW.sequences.cutadapt.fasta"

# cutadapt -b CNNNNNNNNVTTTTTTTTTTTTTTTTTTTTTVN -b GNNNNNNNNVAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWGX.cutinfo.txt" "../Fungal_First_Run/AYWGX/AYWGX.fastq" > "./AYWGX.sequences.cutadapt.fasta"

# cutadapt -b CNNNNNNNNVTTTTTTTTTTTTTTTTTTTTTVN -b GNNNNNNNNVAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWGY.cutinfo.txt" "../Fungal_First_Run/AYWGY/AYWGY.fastq" > "./AYWGY.sequences.cutadapt.fasta"

# cutadapt -b CNNNNNNNNVTTTTTTTTTTTTTTTTTTTTTVN -b GNNNNNNNNVAAAAAAAAAAAAAAAAAAAAAVN --info-file="./cutinfo_individual/AYWGZ.cutinfo.txt" "../Fungal_First_Run/AYWGZ/AYWGZ.fastq" > "./AYWGZ.sequences.cutadapt.fasta"



